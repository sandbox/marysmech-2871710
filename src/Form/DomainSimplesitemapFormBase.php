<?php

namespace Drupal\domain_simple_sitemap\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\domain_simple_sitemap\DomainSimplesitemap;
use Drupal\Core\Path\PathValidator;

/**
 * Class DomainSimplesitemapFormBase.
 *
 * @package Drupal\domain_simple_sitemap\Form
 */
abstract class DomainSimplesitemapFormBase extends ConfigFormBase {

  protected $generator;
  protected $formHelper;
  protected $pathValidator;

  /**
   * SimplesitemapFormBase constructor.
   *
   * @param \Drupal\domain_simple_sitemap\DomainSimplesitemap $generator
   * @param \Drupal\domain_simple_sitemap\Form\DomainFormHelper $form_helper
   * @param \Drupal\Core\Path\PathValidator $path_validator
   */
  public function __construct(
    DomainSimplesitemap $generator,
    DomainFormHelper $form_helper,
    PathValidator $path_validator
  ) {
    $this->generator = $generator;
    $this->formHelper = $form_helper;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('domain_simple_sitemap.generator'),
      $container->get('domain_simple_sitemap.form_helper'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['domain_simple_sitemap.settings'];
  }

  /**
   *
   */
  protected function getDonationText() {
    return "<div class='description'>" . $this->t("If you would like to say thanks and support the development of this module, a <a target='_blank' href='@url'>donation</a> is always appreciated.", ['@url' => 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5AFYRSBLGSC3W']) . "</div>";
  }

}
