<?php

namespace Drupal\domain_simple_sitemap\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\domain_simple_sitemap\DomainSimplesitemap;

/**
 * Class DomainSimplesitemapController.
 *
 * @package Drupal\domain_simple_sitemap\DomainController
 */
class DomainSimplesitemapController extends ControllerBase {

  /**
   * The sitemap generator.
   *
   * @var \Drupal\domain_simple_sitemap\DomainSimplesitemap
   */
  protected $generator;

  /**
   * SimplesitemapController constructor.
   *
   * @param \Drupal\domain_simple_sitemap\DomainSimplesitemap $generator
   *   The sitemap generator.
   */
  public function __construct(DomainSimplesitemap $generator) {
    $this->generator = $generator;
  }

  /**
   * Returns the whole sitemap, a requested sitemap chunk, or the sitemap index file.
   *
   * @param int $chunk_id
   *   Optional ID of the sitemap chunk. If none provided, the first chunk or
   *   the sitemap index is fetched.
   *
   * @throws NotFoundHttpException
   *
   * @return object
   *   Returns an XML response.
   */
  public function getSitemap($chunk_id = NULL) {
    $output = $this->generator->getSitemap($chunk_id);
    if (!$output) {
      throw new NotFoundHttpException();
    }

    // Display sitemap with correct XML header.
    $response = new CacheableResponse($output, Response::HTTP_OK, ['content-type' => 'application/xml']);
    $meta_data = $response->getCacheableMetadata();
    $meta_data->addCacheTags(['simple_sitemap']);
    $meta_data->addCacheContexts(['url.site']);
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('domain_simple_sitemap.generator'));
  }

}
